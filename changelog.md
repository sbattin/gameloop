Version 0.0.9 (Latest)
======================
 * Basic functional parts, drawables and updatables, are in place
 * Some example components exist for demo purposes
 * Some callback logic exists for constructing asynchronously
 * Not enough to call a 0.1.0 release.

Roadmap
=======
A rough plan for the future.
0.1.0
-----
 * Clean up garbage in the repo

0.2.0
-----
 * Separate intializers, extra classes, and globals from the loop
 * remove embarrassing "prev-event save, set event" pattern and replace with addevent listener
 * implement window visibility-loss detection
 * Document API and usage
 * Allow rendering contexts to be contained within components, rather than reusing a single one
 * Pausing, deliberate and when focus is lost
 * Prevent problems due to long times between renderanimationframes
 * Add ability to vary update rate, update without drawing, draw without updating
...

1.0.0
-----
 * Formal input
 * Audio
 * Example projects
 * Data Storage
 * SpriteBatching
 * Networking
 * Dynamic component inclusion (a la require.js)
 * Project packaging for quick deployment or distribution

Old Versions
============
Nothing to see here...